# python visualization of LOTR script
---------
<img src="img/demo.jpg"  width="500" height="400">

## Introduction
This project was developed as part of a course at FH JOANNEUM.

This project is inspired by the following Kaggle notebooks:
- https://www.kaggle.com/paultimothymooney/exploring-the-lord-of-the-rings-dataset
- https://www.kaggle.com/xvivancos/analyzing-the-lord-of-the-rings-data
- https://www.kaggle.com/xvivancos/analyzing-star-wars-movie-scripts
- https://www.kaggle.com/andradaolteanu/rickmorty-scripts
- https://github.com/tianyigu/Lord_of_the_ring_project

The data was used from the Kaggle challange:
- https://www.kaggle.com/paultimothymooney/lord-of-the-rings-data

Main libs in the project:
- https://hvplot.holoviz.org/index.html
- https://panel.holoviz.org/index.html

## Goal
The aim of this project was to build a small exemplary dashboard. Therefore, data from the Lord of the Rings trilogy was analyzed and presented. The goal is to create all representations and dashboard components in Python and in a Jupyter notebook, respectively, as this should reflect a fast prototype workflow. The main tools are: hvplot and panel.

![Yolo](img/YOLO.gif)

## Setup

### Step 1
Please use Python3.8 or higher

macOS/Linux:
```{bash}
git clone https://gitlab.com/laszloBalo/python-visualization-of-lotr-script.git
cd python-visualization-of-lotr-script
python3.8 -m venv venv
source venv/bin/activte
pip install -r requirement.txt
```

### Step 2
- Start "text_analysis.ipynb"
- execute all cells
- Hint: last cell start bokeh server


## QA
### LookupError -> ntlk
```
LookupError:
**********************************************************************
  Resource stopwords not found.
  Please use the NLTK Downloader to obtain the resource:

import nltk
nltk.download('stopwords')
```

change into python terminal and manually download nltk dependencies (In the given python enviroment)
```{bash}
python
>>> import nltk
>>> nltk.download()
```
Now a GUI should open up where you should download all dependencies -> press download button
